#!/bin/bash

# Checking for inputs.
if [[ $1 == "" ]]
then
	echo "Must give input image!"

	exit 1
fi

# Making the temp tile directories if they don't already exist.
if [ ! -d inputtiles/ ]
then
	mkdir inputtiles/
fi

if [ ! -d outputtiles/ ]
then
	mkdir outputtiles/
fi

# Removing the intermediate files.
rm -f $1.ppm
rm -f inputtiles/*
rm -f outputtiles/*

echo "Scaling input image to resolutions divisible by 8..."

# Converting the image to a ppm.
convert $1 -depth 8 -type truecolor -strip $1.ppm

# Getting the image resolution.
iwidth=$(file $1.ppm | tr ',' '\n' | head -n 2 | tail -n 1 | awk '{print $3}')
iheight=$(file $1.ppm | tr ',' '\n' | head -n 2 | tail -n 1 | awk '{print $5}')

# Calculating the 8-divisible image resolutions.
siwidth=$(($iwidth - $(($iwidth % 8))))
siheight=$(($iheight - $(($iheight % 8))))

# Scaling the image's resolutions down to their nearest multiple of 8.
convert $1.ppm -depth 8 -type truecolor -strip -scale ${siwidth}x${siheight}! $1.ppm

echo "Breaking input image into tiles..."

# Breaking the input image into 8x8 tiles.
convert $1.ppm -depth 8 -type truecolor -strip -crop 8x8 +adjoin inputtiles/itile_%09d.ppm

echo "Working on tiles..."

### Main loop.
for i in $(ls inputtiles/)
do
	echo "./tileFinder inputtiles/$i charsets/column/codepage437.ppm outputtiles/out_$i"
done | parallel

echo "Stacking finished tiles into output image \"$1.ASCII.png\" ..."

# Stack finished tiles into output images.
montage -mode concatenate -tile $(($(($iwidth - $(($iwidth % 8)))) / 8))x outputtiles/out_itile_*.ppm $1.ASCII.png

# Removing the intermediate files.
rm -f $1.ppm
rm -f inputtiles/*
rm -f outputtiles/*

exit 0
