## About

An image filter/converter for turning pictures into ASCII art with sub-character-cell fidelity by trying to find similar color and symbol patterns that match the original spot on the image better than simple overall brightness mapping.

This program uses codepage 437 with a 4-bit color palette by default.

![input image](./samples/in.jpg)

![output image](./samples/out.png)

## Requirements

imagemagick convert

GNU parallel

## Building

```bash
./compile.sh
```

## Usage

```bash
./img2char.sh IMAGE
```

## Extras

Any binary 24-bit PPM image with resolution 8 x 2048 will work for the character set file.

### Limitations

Creates another image, not actual text.

Doesn't work for input images which would break into more 8x8 tiles than bash's argument list can handle.

## License

See LICENSE.txt
