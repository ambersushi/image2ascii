#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
	// Checking if enough args.
	if(argc != 4)
	{
		printf("Wrong number of args!\n");

		return 1;
	}

	// Open input tile.
	FILE * ip = fopen(argv[1], "rb");

	// Open character file.
	FILE * cp = fopen(argv[2], "rb");

	// Input tile buffer.
	u_int8_t inputtile[8 * 8 * 3];

	// Buffer for the whole character tile.
	u_int8_t allcharbuf[8 * 8 * 3 * 256];

	// Reading past the header of the input file.
	fscanf(ip, "P6\n8 8\n255\n");

	// Reading the input file into its buffer.
	fread(inputtile, sizeof(u_int8_t) * 3, 8 * 8, ip);

	// Reading past the header of the character file.
	fscanf(cp, "P6\n8 2048\n255\n");

	// Reading the character image into its buffer.
	fread(allcharbuf, sizeof(u_int8_t) * 3, 8 * 8 * 256, cp);

	// Closing input files.
	fclose(ip);
	fclose(cp);

	// Buffer for holding a character tile temporarily.
	u_int8_t sincharbuf[8 * 8 * 3];

	// For holding the indecies of the most similar character, background, and foreground tiles, respectively.
	int mostsimchindex = 0;
	int mostsimbgindex = 0;
	int mostsimfgindex = 0;

	// For holding the current smallest difference of the tiles.
	int smallestdiff = 1000000000;

	// Buffer for holding the output tile.
	u_int8_t outtilebuf[8 * 8 * 3];

	// For holding the color pallete. (C64 color pallete.)
	/*
	u_int8_t colpallete[3 * 16] = {
		0x00, 0x00, 0x00,
		0xFF, 0xFF, 0xFF,
		0x9F, 0x4E, 0x44,
		0x6A, 0xBF, 0xC6,
		0xA0, 0x57, 0xA3,
		0x5C, 0xAB, 0x5E,
		0x50, 0x45, 0x9B,
		0xC9, 0xD4, 0x87,
		0xa1, 0x68, 0x3c,
		0x6D, 0x54, 0x12,
		0xCB, 0x7E, 0x75,
		0x62, 0x62, 0x62,
		0x89, 0x89, 0x89,
		0x9A, 0xE2, 0x9B,
		0x88, 0x7E, 0xCB,
		0xAD, 0xAD, 0xAD
	};
	*/

	// For holding the color pallete. (4-bit color pallete.)
	u_int8_t colpallete[3 * 16] = {
		0xff, 0x30, 0x16,
		0x00, 0xf9, 0x2c,
		0x00, 0x27, 0xfb,
		0x00, 0xfc, 0xfe,
		0xff, 0x3f, 0xfc,
		0xff, 0xfd, 0x33,
		0xff, 0xff, 0xff,
		0x9b, 0x17, 0x08,
		0x00, 0x8f, 0x15,
		0x00, 0x12, 0x90,
		0x00, 0x90, 0x92,
		0x9a, 0x20, 0x91,
		0x94, 0x91, 0x19,
		0xb8, 0xb8, 0xb8,
		0x68, 0x68, 0x68,
		0x00, 0x00, 0x00
	};

	// For tallying up the total difference between the constructed tile and original tile.
	int diff = 0;

	// Main loop.
	// Looping over background color.
	for(int bg = 0; bg < 16 * 3; bg += 3)
	{
		// Looping over foreground color.
		for(int fg = 0; fg < 16 * 3; fg += 3)
		{
			if(bg == fg)
			{
				continue;
			}

			// Looping over character.
			for(int ch = 0; ch < 8 * 8 * 3 * 256; ch += (8 * 8 * 3))
			{
				// Setting the background color.
				for(int i = 0; i < 8 * 8 * 3; i += 3)
				{
					memcpy(&outtilebuf[i], &colpallete[bg], sizeof(u_int8_t) * 3);
				}

				// Copying the current character to its temp buffer.
				memcpy(sincharbuf, &allcharbuf[ch], sizeof(u_int8_t) * 8 * 8 * 3);

				// Adding the character in the foreground color.
				for(int i = 0; i < 8 * 8 * 3; i += 3)
				{
					if(sincharbuf[i] != 0x00)
					{
						memcpy(&outtilebuf[i], &colpallete[fg], sizeof(u_int8_t) * 3);
					}
				}
				
				// Getting the overall diff of the constructed tile and original tile.
				for(int i = 0; i < 8 * 8 * 3; i++)
				{
					// Least squares vs linear difference.
					diff += (inputtile[i] - outtilebuf[i]) * (inputtile[i] - outtilebuf[i]);
					//diff += abs((u_int8_t)inputtile[i] - (u_int8_t)outtilebuf[i]);
				}

				// Recording the index of the most similar character, background color, and foreground color.
				if(diff < smallestdiff)
				{
					smallestdiff = diff;

					mostsimbgindex = bg;
					mostsimfgindex = fg;
					mostsimchindex = ch;
				}

				// Resetting the total tile difference counter.
				diff = 0;
			}
		}
	}

	// Recreating the most similar character from saved indecies.
	// Setting the background color.
	for(int i = 0; i < 8 * 8 * 3; i += 3)
	{
		memcpy(&outtilebuf[i], &colpallete[mostsimbgindex], sizeof(u_int8_t) * 3);
	}

	// Copying the current character to its temp buffer.
	memcpy(sincharbuf, &allcharbuf[mostsimchindex], sizeof(u_int8_t) * 8 * 8 * 3);

	// Adding the character in the foreground color.
	for(int i = 0; i < 8 * 8 * 3; i += 3)
	{
		if(sincharbuf[i] != 0x00)
		{
			memcpy(&outtilebuf[i], &colpallete[mostsimfgindex], sizeof(u_int8_t) * 3);
		}
	}

	// Opening output files.
	FILE * op = fopen(argv[3], "w+");

	// Writing out output tile headers.
	fprintf(op, "P6\n8 8\n255\n");

	// Writing out output tile data.
	fwrite(outtilebuf, sizeof(u_int8_t) * 8 * 8 * 3, 1, op);

	// Closing output files.
	fclose(op);

	return 0;
}
